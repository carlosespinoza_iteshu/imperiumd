﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImperiumD
{
    public class Lectura
    {
        public DateTime FechaHora { get; set; }
        public string ProjectId { get; set; }
        public string ReadingID { get; set; }
        public string SensorId { get; set; }
        public float Valor { get; set; }
        public override string ToString()
        {
            return "Valor: " + Valor.ToString() + " Fecha/Hora: " + FechaHora.ToString();
        }
    }
}
