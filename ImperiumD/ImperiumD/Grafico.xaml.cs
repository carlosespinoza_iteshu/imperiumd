﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ImperiumD
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Grafico : ContentPage
    {
        public PlotModel model { get; private set; }
        List<Lectura> lecturasRitmoCardiaco = new List<Lectura>();
        List<Lectura> lecturasGlucosa = new List<Lectura>();
        string urlCardiaco = "api/Readings?userEmail=a14021455@iteshu.edu.mx&password=12345&sensorId=1d53dfa6-1b0f-4518-a8b9-16b6bebd9c88&numLecturas=10";
        string urlGlucosa = "api/Readings?userEmail=a14021455@iteshu.edu.mx&password=12345&sensorId=8b1b19b4-2b90-4eef-9093-28f7d1d3bf4d&numLecturas=10";
        private HttpClient _cliente;
        public Grafico()
        {
            InitializeComponent();
            _cliente = new HttpClient();
            _cliente.BaseAddress = new Uri("http://iotcloudrest.azurewebsites.net/");
            _cliente.DefaultRequestHeaders.Clear();
            _cliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _cliente.MaxResponseContentBufferSize = 256000;
            

        }

        private async Task ObtenerLecturasAsync()
        {
            lblDatos.Text = "Obteniendo datos...";
            HttpResponseMessage responseCardiaco = await _cliente.GetAsync(urlCardiaco);
            if (responseCardiaco.IsSuccessStatusCode)
            {
                var content = await responseCardiaco.Content.ReadAsStringAsync().ConfigureAwait(false);
                lecturasRitmoCardiaco = JsonConvert.DeserializeObject<List<Lectura>>(content);
                lblDatos.Text = "RC: " + lecturasRitmoCardiaco.Count;
            }
            else
            {
                lecturasRitmoCardiaco = null;
                lblDatos.Text = "RC: Error ";
            }

            HttpResponseMessage responseGlucosa = await _cliente.GetAsync(urlGlucosa);

            if (responseGlucosa.IsSuccessStatusCode)
            {
                var content = await responseGlucosa.Content.ReadAsStringAsync().ConfigureAwait(false);
                lecturasGlucosa = JsonConvert.DeserializeObject<List<Lectura>>(content);
                lblDatos.Text += " G: " + lecturasRitmoCardiaco.Count;
            }
            else
            {
                lecturasGlucosa = null;
                lblDatos.Text += " G: Error ";
            }
            if(lecturasRitmoCardiaco==null && lecturasGlucosa == null)
            {
                lblDatos.Text = "Error al obtener los datos";
            }
            else
            {
                GraficaDatos();
                btnMostrar.IsVisible = true;
            }
        }

        private void GraficaDatos()
        {
            List<Lectura> cardiacas = lecturasRitmoCardiaco.OrderByDescending(m => m.FechaHora).Take(10).ToList();
            List<Lectura> glucosas= lecturasGlucosa.OrderByDescending(m => m.FechaHora).Take(10).ToList();
            model = new PlotModel();
            DateTimeAxis ejeX = new DateTimeAxis();
            ejeX.Position = AxisPosition.Bottom;
            LinearAxis ejeY = new LinearAxis();
            ejeY.Position = AxisPosition.Left;
            model.Axes.Add(ejeX);
            model.Axes.Add(ejeY);
            model.Title = "Reporte de lecturas";

            LineSeries lCardiaca = new LineSeries();
            lCardiaca.Title = "Ritmo cardiaco";
            lCardiaca.DataFieldX = "FechaHora";
            lCardiaca.DataFieldY = "Valor";
            lCardiaca.ItemsSource = cardiacas;
            model.Series.Add(lCardiaca);

            LineSeries lGlucosa = new LineSeries();
            lGlucosa.Title = "Glucosa";
            lGlucosa.DataFieldX = "FechaHora";
            lGlucosa.DataFieldY = "Valor";
            lGlucosa.ItemsSource = glucosas;
            model.Series.Add(lGlucosa);

            chart.Model = model;
        }

        private async void btnActualizar_Clicked(object sender, EventArgs e)
        {
            await ObtenerLecturasAsync();
        }

        private void btnMostrar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Mostrar(lecturasRitmoCardiaco.OrderByDescending(m => m.FechaHora).ToList(), lecturasGlucosa.OrderByDescending(m => m.FechaHora).ToList()));
        }
    }
}