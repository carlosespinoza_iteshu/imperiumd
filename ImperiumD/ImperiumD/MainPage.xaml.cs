﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ImperiumD
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void btnIniciar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Grafico());
        }

        private void btnAcerca_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("ImperiumD", "Texto...", "Aceptar");
        }
    }
}
