﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ImperiumD
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Mostrar : ContentPage
    {
        public Mostrar(List<Lectura>Cardiaco, List<Lectura> Glucosa)
        {
            InitializeComponent();
            lstCardiaco.ItemsSource = Cardiaco;
            lstGlucosa.ItemsSource = Glucosa;
        }
    }
}